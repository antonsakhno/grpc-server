FROM openjdk:11
COPY target/webflux-grpc-poc-0.0.1-SNAPSHOT.jar webflux-grpc-poc-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/webflux-grpc-poc-0.0.1-SNAPSHOT.jar"]