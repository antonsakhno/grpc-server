package com.cashir.webfluxgrpcpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxGrpcApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxGrpcApplication.class, args);
    }

}
