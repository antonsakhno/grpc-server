package com.cashir.webfluxgrpcpoc;

import com.cashir.webfluxgrpcpoc.proto.TestRequest;
import com.cashir.webfluxgrpcpoc.proto.TestResponse;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
public class TestRepository {

    public TestResponse getResponse(TestRequest request) {
        log.info(request);
        return TestResponse.newBuilder()

                .setTimestamp(System.currentTimeMillis())
                .setPropertyName1(Optional.ofNullable(request).map(TestRequest::getPropertyName1).orElse(null))
                .build();
    }
}
