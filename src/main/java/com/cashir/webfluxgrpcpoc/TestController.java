package com.cashir.webfluxgrpcpoc;

import com.cashir.webfluxgrpcpoc.proto.TestRequest;
import com.cashir.webfluxgrpcpoc.proto.TestResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@AllArgsConstructor
public class TestController {

    private TestRepository repository;

    @PostMapping
    public TestResponse getResponse() {
        return repository.getResponse(TestRequest.getDefaultInstance());
    }

    @GetMapping
    public long getTimestamp() {
        return System.currentTimeMillis();
    }
}
