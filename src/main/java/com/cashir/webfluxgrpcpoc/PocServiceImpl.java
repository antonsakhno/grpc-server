package com.cashir.webfluxgrpcpoc;

import com.cashir.webfluxgrpcpoc.proto.PoCServiceGrpc.PoCServiceImplBase;
import com.cashir.webfluxgrpcpoc.proto.TestRequest;
import com.cashir.webfluxgrpcpoc.proto.TestResponse;
import com.google.protobuf.Any;
import com.google.rpc.Code;
import com.google.rpc.ErrorInfo;
import io.grpc.protobuf.StatusProto;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.devh.boot.grpc.server.service.GrpcService;

@Log4j2
@GrpcService
@AllArgsConstructor
public class PocServiceImpl extends PoCServiceImplBase {
    private TestRepository repository;

    @Override
    public void test(TestRequest request, StreamObserver<TestResponse> responseObserver) {
        if("error".equals(request.getPropertyName1())) {
            log.info("error matched");
            generateError(request, responseObserver);
        } else {
            responseObserver.onNext(repository.getResponse(request));
            responseObserver.onCompleted();
        }
    }

    private void generateError(TestRequest request, StreamObserver<TestResponse> responseObserver) {
        com.google.rpc.Status status = com.google.rpc.Status.newBuilder()
                .setCode(Code.ABORTED.getNumber())
                .setMessage("error patern match")
                .addDetails(Any.pack(ErrorInfo.newBuilder()
                                             .setReason("error patern match")
                                             .setDomain("com.cashir.webfluxgrpcpoc")
                                             .putMetadata("fieldValue", request.getPropertyName1())
                                             .build()))
                .build();
        responseObserver.onError(StatusProto.toStatusException(status));
    }
}
